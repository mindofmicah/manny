<?php
namespace Mindofmicah\Manny;
use DB;
use Response;
use Input;
use stdClass;
class AdminApiController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($table = null)
	{
		if(is_null($table)) {
            return 'home page';
        }

        $data = json_decode(file_get_contents(app_path('data.json')));
        if (empty($data->tables->$table)) {
            return 'not a valid table';
        }
        $model_class = $data->tables->$table->model;
        return $model_class::all();

        // else
        //   throw an error
        return $table . ' admin page';
	}

    public function fields($table)
    {
        $results = (DB::select(DB::raw('PRAGMA table_info("'.$table.'")')));

        return Response::json(array_values(array_map(function ($element) {
            $ret = new stdClass();
            $ret->field     = $element->name;
            $ret->type    = 'textarea';
            $ret->required = !!$element->notnull;
            $ret->default  = $element->dflt_value;

            return $ret;
        }, array_filter($results, function($element){
            return !preg_match('/(id|_at)$/', $element->name);
        }))));
    }



    public function showTable($a)
    {
        dd(func_get_args());
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($table)
	{
        $data = json_decode(file_get_contents(app_path('data.json')));
        if (empty($data->tables->$table)) {
            return 'not a valid table';
        }
        $model_class = $data->tables->$table->model;
        $model = new $model_class;
        foreach(Input::all() as $field=>$value) {
            $model->$field = $value;
        }
        $model->save();
        return Response::json($model);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($table, $id)
	{
	    $data = json_decode(file_get_contents(app_path('data.json')));
        if (empty($data->tables->$table)) {
            return 'not a valid table';
        }
        $model_class = $data->tables->$table->model;
        try {
            $model = $model_class::findOrFail($id);
//return $model;
            return Response::json($model, 200);
        } catch (\Exception $e) {
            return Response::json('Doesn\'t work', 404);
        }
	
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($table, $id)
	{
	    $data = json_decode(file_get_contents(app_path('data.json')));
        if (empty($data->tables->$table)) {
            return 'not a valid table';
        }
        $model_class = $data->tables->$table->model;
        try {
            $model = $model_class::findOrFail($id);
            foreach (Input::all() as $k=>$v) {
                $model->$k = $v;
            }
            $model->save();
            return Response::json($model, 200);
        } catch (\Exception $e) {
            return Response::json('Doesn\'t work: ' . $e->getMessage(), 404);
        }
	//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($table, $id)
	{
        $data = json_decode(file_get_contents(app_path('data.json')));
        if (empty($data->tables->$table)) {
            return 'not a valid table';
        }
        $model_class = $data->tables->$table->model;
        try {
            $model = $model_class::findOrFail($id);
            $model->delete();
            return Response::json($model, 200);
        } catch (\Exception $e) {
            return Response::json('Doesn\'t work', 404);
        }

	}


}
