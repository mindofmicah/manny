<?php namespace Mindofmicah\Manny;

use Mindofmicah\Manny\Commands\MannyAssetsCommand;
use Mindofmicah\Manny\Commands\MannyInstallCommand;
use Mindofmicah\Manny\Commands\MannyPopulateDataCommand;

use Illuminate\Support\ServiceProvider;

class MannyServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('mindofmicah/manny');
        require __DIR__ . '/routes.php';
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
        $this->app['manny.install'] = $this->app->share(function ($app) {
            return new MannyInstallCommand;
        });
        $this->app['manny.populate'] = $this->app->share(function ($app) {
            return new MannyPopulateDataCommand;
        });

        $this->app['manny.assets'] = $this->app->share(function ($app) {
            return new MannyAssetsCommand;
        });

        $this->commands('manny.populate');
        $this->commands('manny.install');
        $this->commands('manny.assets');
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

}
