<?php
Route::group(['prefix'=>Config::get('manny::routing.route')], function () {
$controller = 'Mindofmicah\Manny\AdminApiController';
$phpcontroller = 'Mindofmicah\Manny\AdminController';
    Route::get('{taco}', $controller . '@index');
    Route::post('{taco}', $controller . '@store');
    Route::delete('{taco}/{id}', $controller. '@destroy');
    Route::put('{taco}/{id}', $controller . '@update');
    Route::get('{taco}/fields', $controller . '@fields');
    Route::get('{taco}/{id}', $controller . '@show')->where('id', '[0-9]+');
    Route::controller('', $phpcontroller);
    Route::get('', $phpcontroller . '@index');
});
