<?php
namespace Mindofmicah\Manny\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class MannyPopulateDataCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'manny:populate';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

    public function initData()
    {
        if (file_exists(app_path('data.json'))) {
            $file = json_decode(file_get_contents(app_path('data.json')));
        } else {
            $file = new \stdClass;
        }
        return $file;
    }

    private function grabModels()
    {
        return array_map(function ($filename) {
            preg_match('/\/([^.\/]+?)\.php$/', $filename, $match);
            return $match[1];
        }, glob(app_path('models/*')));
    }

    public function addTableInformation(&$data)
    {
        if (empty($data->tables)) {
            $data->tables = [];
        }
        
        foreach ($this->grabModels() as $model_name) {
            $input = $this->ask('Would you like to include ' . $model_name .'?', 'y');
            if ($input != 'n') {
                $key = strtolower($model_name) . 's';
                $key = $this->ask('what would you like the key to be ['.$key.']', $key);
                if (array_key_exists($key, $data->tables)) {
                    if ('y' == $this->ask($key . ' already exists, replace it[yN]')) {
                        $data->tables[$key] = ['model' => '\\' . $model_name];
                    }
                } else {
                    $data->tables[$key] = ['model' => '\\' . $model_name];
                }
            }
        }
    }

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{   
        $data = $this->initData();
        $this->addTableInformation($data);
        $this->saveData($data);
	}

    public function saveData($data)
    {
        file_put_contents(app_path('data.json'), json_encode($data, JSON_PRETTY_PRINT));
    }
	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
//			array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
//			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
