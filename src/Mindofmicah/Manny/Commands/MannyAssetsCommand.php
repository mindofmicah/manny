<?php
namespace Mindofmicah\Manny\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class MannyAssetsCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'manny:assets';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        if (!$this->option('no-assets')) {
            $this->call('asset:publish', ['package' => 'mindofmicah/manny']);
        }

        file_put_contents('.bowerrc', '{"directory" : "public/vendor"}');

        exec('bower install https://bitbucket.org/mindofmicah/mannyjs.git', $output);
        
        $this->line(implode("\n", $output));
        file_put_contents('public/vendor/mannyjs/.bowerrc', '{"directory" : "vendor"}');
        exec('cd public/vendor/mannyjs && npm install 2>&1 | grep "npm ERR!"', $output);
        if (count($output)) {
            $this->info('Looks like we need to run npm install as an admin');
            exec('cd public/vendor/mannyjs && sudo npm install 2>&1', $output);
        exec('cd public/vendor/mannyjs && bower install 2>&1', $output);
        }
        $this->line(implode("\n",$output));
        exec('cd public/vendor/mannyjs && bower install 2>&1', $output);
        $this->line(implode("\n",$output));
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
//			array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('no-assets', null, InputOption::VALUE_NONE, 'An example option.', null),
		);
	}

}
